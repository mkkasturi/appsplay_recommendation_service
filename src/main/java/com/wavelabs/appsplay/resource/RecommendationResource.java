package com.wavelabs.appsplay.resource;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wavelabs.appsplay.service.RecommendationService;

@RestController
@Component
public class RecommendationResource {
	
	@Autowired
	RecommendationService recommendationService;

	@RequestMapping(value = "api/appsplay/campaigns/{id}", method = RequestMethod.GET)
	public void getAllCampaigns(@PathVariable("id") String id) throws IOException {
		recommendationService.getCampaigns(id);
	}
}
