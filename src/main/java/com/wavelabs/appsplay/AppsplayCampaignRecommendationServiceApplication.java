package com.wavelabs.appsplay;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.wavelabs.appsplay.service.RecommendationService;

@Configuration
@SpringBootApplication
@ComponentScan({"com.wavelabs.appsplay.service, com.wavelabs.appsplay.resource"})
@EnableAutoConfiguration
public class AppsplayCampaignRecommendationServiceApplication {
	
	@Autowired
	RecommendationService recommendationService;
	public static void main(String[] args) {
		SpringApplication.run(AppsplayCampaignRecommendationServiceApplication.class, args);
	}
}
