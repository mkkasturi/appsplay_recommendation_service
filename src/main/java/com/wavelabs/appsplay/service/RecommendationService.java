package com.wavelabs.appsplay.service;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Component;


@Component
public class RecommendationService {
	 public void getCampaigns(String uuid) throws IOException{
		// Instantiating Configuration class
	      Configuration config = HBaseConfiguration.create();
	      
	      config.set("zookeeper.znode.parent", "/hbase");
	      config.set("hbase.master", "192.168.99.100:60000");
	      //config.set("hbase.zookeeper.quorum", "nn01.itversity.com,nn02.itversity.com,rm01.itversity.com");// ip of hbase server(remote)
	      config.set("hbase.zookeeper.quorum", "192.168.99.100");// ip of hbase server(remote)
	      config.set("hbase.zookeeper.property.clientPort", "2181");// portno : 2181 default
	      config.set("zookeeper.znode.parent","/hbase-unsecure");
	      config.set("hbase.cluster.distributed","true");
	      
	      Connection connection = ConnectionFactory.createConnection(config);
	      Admin admin  = connection.getAdmin();
	      
	      // Instantiating HTable class
	      Table table = connection.getTable(TableName.valueOf("appsplay_campaigns"));

	      // Instantiating Get class
	      Get id = new Get(Bytes.toBytes(uuid));
	      
	      // Reading the data
	      Result result = table.get(id);
	      
	      // Reading values from Result class object
	      byte [] value = result.getValue(Bytes.toBytes("target"),Bytes.toBytes("device_model"));
	      
	      
	      // Printing the values
	      String deviceModel = Bytes.toString(value);	      
	      System.out.println("device Model: " + deviceModel);

	 }
	 
}
